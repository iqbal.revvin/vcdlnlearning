<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMateriTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materi', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('mapel_id');
            $table->string('kategori');
            $table->string('file_video');
            $table->string('url_video_youtube');
            $table->string('file_rpp');
            $table->string('flow_diagram');
            $table->string('story_board');
            $table->text('deskripsi');
            $table->string('tindakan');
            $table->string('keterangan');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('mapel_id')->references('id')->on('mapel');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('materi');
    }
}
