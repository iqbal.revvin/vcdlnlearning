@extends('master')

@section('content')
<section class="relative w-full h-full py-40 min-h-screen">
  <div class="absolute top-0 w-full h-full bg-blueGray-800 bg-full bg-no-repeat"
    style="background-image: url(/images/bg_register.png)" />
  <div class="container mx-auto px-4 h-full">
    <div class="flex content-center items-center justify-center h-full">
      <div class="w-full lg:w-4/12 px-4">
        <div
          class="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-blueGray-200 border-0">
          <div class="rounded-t mb-0 px-6 py-6">
            <div class="text-center mb-3">
              <h6 class="text-blueGray-500 text-sm font-bold">
                Masuk Akun
              </h6>
            </div>
            <hr class="mt-6 border-b-1 border-blueGray-300" />
          </div>
          <div class="flex-auto px-4 lg:px-10 py-10 pt-0">
            <div class="text-blueGray-400 text-center mb-3 font-bold">
              <small>masuk dengan akun terdaftar</small>
            </div>
            <form role="form" method="POST" action="{{ route('backpack.auth.login') }}">
              {!! csrf_field() !!}
              <div class="relative w-full mb-3">
                <label class="block uppercase text-blueGray-600 text-xs font-bold mb-2" for="email">Email</label>
                <input type="email" placeholder="Email" name="{{ $username }}" value="{{ old($username) }}" id="{{ $username }}" 
                      class="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 rounded 
                        text-sm shadow focus:outline-none focus:ring w-full ease-linear 
                        transition-all duration-150 {{ $errors->has($username) ? 'border-2 border-red-500' : '' }}" 
                />
                @if ($errors->has($username))
                  <span class="mt-2 ml-1 text-xs text-red-500">
                    <strong>{{ $errors->first($username) }}</strong>
                  </span>
                @endif
              </div>
              
              <div class="relative w-full mb-3">
                <label class="block uppercase text-blueGray-600 text-xs font-bold mb-2" for="password">Kata Sandi</label>
                <input type="password" placeholder="Kata Sandi" name="password"
                      class="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 rounded 
                        text-sm shadow focus:outline-none focus:ring w-full ease-linear 
                        transition-all duration-150 {{ $errors->has('password') ? 'border-2 border-red-500' : '' }}" 
                />
                @if ($errors->has('password'))
                  <span class="mt-2 ml-1 text-xs text-red-500">
                    <strong>{{ $errors->first('password') }}</strong>
                  </span>
                @endif
              </div>
              <div>
                <label class="inline-flex items-center cursor-pointer">
                  <input id="customCheckLogin" type="checkbox" name="remember" class="form-checkbox border-0 rounded text-blueGray-700 ml-1 w-5 h-5 ease-linear transition-all duration-150" />
                  <span class="ml-2 text-sm font-semibold text-blueGray-600">{{ trans('backpack::base.remember_me') }}</span></label>
              </div>
              <div class="text-center mt-6">
                <button type="submit" class="bg-blueGray-800 text-white active:bg-blueGray-600 text-sm font-bold uppercase 
                    px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 w-full ease-linear transition-all duration-150"
                >
                  Masuk
                </button>
              </div>
            </form>
          </div>
        </div>
        <div class="flex flex-wrap mt-6">
          <div class="w-1/2">
            <a href="/" class="text-blueGray-200">
              <small>Kembali Ke Halaman Utama</small>
            </a>
          </div>
          <div class="w-1/2 text-right">
            @if (backpack_users_have_email() && config('backpack.base.setup_password_recovery_routes', true))
            <a href="{{ route('backpack.auth.password.reset') }}" class="text-blueGray-200"><small>Lupa Kata Sandi</small></a>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
  <footer class="absolute w-full bottom-0 bg-blueGray-800 pb-6">
    <div class="container mx-auto px-4">
      <hr class="mb-6 border-b-1 border-blueGray-600" />
      <div class="flex flex-wrap items-center md:justify-between justify-center">
        <div class="w-full md:w-4/12 px-4">
          <div class="text-sm text-white font-semibold py-1 text-center md:text-left">
            Copyright © <span id="get-current-year"></span>
            <a href="/" class="text-white hover:text-blueGray-300 text-sm font-semibold py-1">VCDLN Learning</a>
          </div>
        </div>
        <div class="w-full md:w-8/12 px-4">
          <ul class="flex flex-wrap list-none md:justify-end justify-center">
            <li>
              <a href="https://appcreation.web.id"
                class="text-white hover:text-blueGray-300 text-sm font-semibold block py-1 px-3">MIT License |
                AppCreation
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </footer>
</section>
</main>
@endsection