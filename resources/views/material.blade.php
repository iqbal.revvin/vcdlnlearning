@extends('master')

@section('content')
    <div id='material-render'>
        <div wire:loading class="fixed top-0 left-0 right-0 bottom-0 w-full h-screen z-50 overflow-hidden bg-gray-700 opacity-75 flex flex-col items-center justify-center">
            <div class="loader ease-linear rounded-full border-4 border-t-4 border-gray-500 h-12 w-12 mb-4"></div>
            <h2 class="text-center text-white text-xl font-semibold">Mohon Tunggu...</h2>
            <p class="w-1/3 text-center text-white">Pemuatan konten bisa lebih lama, tergantung kecepatan jaringan anda.</p>
            <p class="w-1/3 text-center text-white text-xs">Muat ulang halaman, jika konten terlalu lama muncul.</p>
        </div>
    </div>
@endsection