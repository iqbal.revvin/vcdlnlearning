@extends('errors.layout')

@php
  $error_number = 503;
@endphp

@section('title')
  Sistem Dalam Perawatan
@endsection

@section('description')
  @php
    $default_error_message = "The server is overloaded or down for maintenance. Please try again later.";
  @endphp
  Sistem sedang dalam peningkatan kualitas, <br /> mohon untuk kembali lagi beberapa saat kemudian <br />
  <i>{!! isset($exception)? ($exception->getMessage()?$exception->getMessage():$default_error_message): $default_error_message !!}</i>
@endsection
