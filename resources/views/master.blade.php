<!DOCTYPE html>
{{-- <html lang="{{ str_replace('_', '-', app()->getLocale()) }}"> --}}

<html lang="id">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <meta name="theme-color" content="#000000">
        {{-- <meta name="robots" content="index,follow"> --}}
        <meta name="robots" content="noindex, nofollow" />
        <meta name="title" content="VCDLNLearning">
        <meta name="description" content="Virtual Community Digital Learning Nusantara | Temukan & bagikan konten digital pembelajaran untuk seluruh jenjang sekolah di indonesia, serta jangkau konten dengan mudah di berbagai platform yang anda genggam">
        <meta name="keywords" content="vcdlnlearning, pembelajaran online, video belajar gratis, tutorial belajar gratis, aplikasi sekolah, belajar online">
        <meta name="author" content="vcdlnlearning">
        <meta name="google-site-verification" content="EYPD272qLP5lw46eALdEsVKxP0uogcbxsdWqkTssniU">
        <link rel="apple-touch-icon" href="{{asset('pavicon.png')}}">
        <link rel="icon" href="{{asset('pavicon.png')}}">
        @if($title)
            <title>{{env('APP_NAME')}} || {{$title}}</title>
        @else
            <title>{{env('APP_NAME')}}</title>
        @endif
 
        <!-- Fonts -->
        <link href="{{ mix('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/loader.css') }}" rel="stylesheet">
        <link href="{{ asset('webfonts/all.min.css')}}" rel="stylesheet">
        <script src="{{ mix('js/app.js') }}" defer></script>
        <script src="{{ mix('js/module/app.js') }}" defer></script>
    </head>

    <body>
        @yield('content')
    </body>

</html>