import axios from 'axios'

//BASE-URL
const BASE_URL = process.env.MIX_API_URL

//API-KEY
const API_KEY = process.env.MIX_API_KEY

//ENDPOINT
export const REGISTER = 'auth/register'
export const SEKOLAH_SEARCH = 'school/search'
export const CATEGORY = 'data-master/category'

//Subject Area
export const SUBJECT_LIST = 'subject/list'
export const CATEGORY_FILTER_LIST = 'subject/list-category'
export const MAPEL_FILTER_LIST = 'subject/list-mapel'

const API = axios.create({
    headers: { 
        'Access-Control-Allow-Origin' : '*',
        'Access-Control-Allow-Methods' : 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
        'x-api-key': API_KEY
      },
    baseURL: BASE_URL
})

export default API