import React from 'react';

const styles = {
    labelSection: 'block uppercase text-blueGray-600 text-xs font-bold mb-2',
    fieldInputSection: 'border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150',
}

const CFormInput = ({type, label, value, placeholder, onChange}) => {
    return (
        <div className="relative w-full mb-3">
            <label className={styles.labelSection} htmlFor="nama_lengkap">
                {label}
            </label>
            <input type={type} value={value} onChange={onChange} className={styles.fieldInputSection} placeholder={placeholder} />
        </div>
    );
}

export default CFormInput;
