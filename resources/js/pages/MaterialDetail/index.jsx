import ReactDOM from 'react-dom'
import DetailMateriScreen from './DetailMateriScreen'

if(document.getElementById('materi-detail-render')){
    ReactDOM.render(<DetailMateriScreen />, document.getElementById('materi-detail-render'));
}