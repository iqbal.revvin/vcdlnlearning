import React, {Component} from 'react'
import {StyleSheet, View } from 'react-native-web'
import tailwind from 'tailwind-rn'
import CNavbar from '../Material/Components/CNavbar'

class DetailMateriScreen extends Component {
    render(){
        return(
            <View styles={tailStyles.container}>
                <CNavbar />
                Detail Materi Screen
            </View>
        )
    }
}

export default DetailMateriScreen

const tailStyles = {
    container : tailwind('relative w-full h-full')
}

const styles = StyleSheet.create({

})