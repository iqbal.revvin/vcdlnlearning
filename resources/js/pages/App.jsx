import ReactDOM from 'react-dom'
import React, { Component } from 'react'
import Data from '../components/Data'

class App extends Component {
    state = {
        count: 0
    }
    render() {
        return (
            <div className=' mx-auto my-4'>
                <div className='antialiased flex flex-row justify-center'>
                    <div className='w-full laptop:w-4/12'>
                        <div className='bg-white rounded py-4 shadow-xl text-center px-2 '>
                            <h2 className='font-bold '>React Test Component</h2>
                            <p>You clicked {this.state.count} times</p>
                        </div>
                        <div className='text-center py-3'>
                            <button className='bg-blue-500 hover:bg-blue-600 active:opacity-50 px-5 py-2 rounded-md text-white focus:outline-none shadow-lg' onClick={() => this.setState({count: this.state.count+1})}>Test Click</button>
                        </div>
                        <Data />
                    </div>
                </div>
            </div>
            // <div className=' antialiased flex items-center justify-center bg-gray-200 dark:bg-gray-700'>
            //     <div className='w-4/12'>
            //         <div className='bg-white dark:bg-gray-800 shadow p-4 rounded-lg mb-5 flex items-center justify-between'>
            //             <div className='font-semibold text-lg text-grey-800 dark:text-white'>Switcher</div>
            //             <div>
            //                 <button onClick={() => selectTheme('light')} className='w-5 h-5 bg-gray-200 mr-2 rounded-full focus:outline-none'></button>
            //                 <button onClick={() => selectTheme('dark')} className='w-5 h-5 bg-black mr-2 rounded-full focus:outline-none'></button>
            //             </div>
            //         </div>
            //         <div className='bg-white dark:bg-gray-800 shadow rounded-lg overflow-hidden'>
            //             <div className='px-10 py-8'>
            //                 <h1 className='text-2xl font-bold text-gray-800 dark:text-white'>Duis consequat consequat eiusmod quis</h1>
            //                 <div className='leading-relaxed text-gray-600 text-lg dark:text-white'>
            //                     Dolor excepteur minim nisi est id eiusmod magna aute id incididunt deserunt ullamco. Reprehenderit irure mollit aute id cupidatat nostrud do aliqua exercitation. Sit magna sit sunt aute cillum duis elit nostrud ipsum.
            //                 </div>
            //             </div>
            //             <div className='px-10 py-6 bg-white dark:bg-gray-900 border-t border-gray-400 dark:border-gray-900 dark:text-gray-200'>
            //                 Dolor excepteur minim nisi est id eiusmod magna
            //             </div>
            //         </div>
            //     </div>
            // </div>
        )
    }
}

export default App
if (document.getElementById('app')) {
    ReactDOM.render(<App />, document.getElementById('app'))
}