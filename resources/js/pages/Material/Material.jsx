import React, { Component } from 'react'
import ConstructionPage from '../../components/ContructionPage'
import { StyleSheet, View, Text, ScrollView } from 'react-native-web'
import tailwind from "tailwind-rn";
import CNavbar from './Components/CNavbar';
import CText from '../../components/Text/CText';
import TextTruncate from 'react-text-truncate';
import CFilterFormContent from './Components/CFilterFormContent';
import { dummyContent } from './dummyContent';
import CContentItem from './Components/CContentItem';
import CModalContainer from '../../components/Modals/CModalContainer';
import CDetailMaterialContent from './Components/CDetailMaterialContent';
import InfiniteScroll from 'react-infinite-scroll-component';
import CLoadingSubject from './Components/CLoadingSubject';
import {getSubjectList} from './action'
class Material extends Component {
  constructor() {
    super()
    this.state = {
      isShowDetailVideo: false,
      isShowLoadingSubject: false,
      subjectData:[],
      detailVideoData:{}
    }
  }

  componentDidMount(){
    this._hadleGetSubject()
  }

  _hadleGetSubject = async () => {
    this.setState({isShowLoadingSubject: true})
    try {
      const response = await getSubjectList()
      if(response?.data?.success){
        this.setState({
          isShowLoadingSubject:false,
          subjectData:response.data.output_data
        })
        console.log(response.data.output_data)
      }else{
        this.setState({isShowLoadingSubject:false})
        alert('Gagal mengambil data, coba untuk mereload browser anda dan pastikan anda terkoneksi ke internet')
      }
    } catch (error) {
      console.log('Material.Material@handleGetSubject', error)
    }
  }

  _handleOnClickVideo = (data) => {
    this.setState({isShowDetailVideo: true, detailVideoData:data})
  }

  render() {
    const { isShowDetailVideo, isShowLoadingSubject, subjectData, detailVideoData } = this.state
    return (
      <View style={tailStyles.container}>
        <CNavbar />
        <div className="bg-white">
          <div className="max-w-7xl mx-auto px-4 sm:px-4 lg:px-4">
            <div className="max-w-2xl mx-auto py-24 md:py-28 lg:py-24 lg:max-w-none">
              <h2 className="text-2xl font-extrabold text-blueGray-700">Materi Room (VCDLNLearning)</h2>
              {/* <div className='mt-6'>
                <CFilterFormContent />
              </div> */}
              <InfiniteScroll dataLength={8} initialLoad={false} 
                next={() => console.log('Load More')} hasMore={true}
                loader={''}
                endMessage={<h2>End Message</h2>}
              >
                {isShowLoadingSubject && (
                  <CSubjectContentLoading />
                )}
                {!isShowLoadingSubject && (
                  <div className="mt-3 space-y-0 grid md:grid md:grid-cols-2 lg:grid lg:grid-cols-4 md:gap-x-6 lg:gap-x-3">
                    {/* {dummyContent.map((callout, i) => (
                      <CContentItem key={i} onClick={() => this.setState({ isShowDetailVideo: true })}
                        id={callout.id} creator={callout.name} thumbnail={`https://picsum.photos/id/${callout.id * Math.floor(Math.random() * 10)}/300/200`}
                        title={callout.description} subject={'Matematika'} />
                    ))} */}
                    {subjectData.map((item, i) => (
                      <CContentItem key={i} onClick={() => this._handleOnClickVideo(item)}
                        id={item.id} creator={item.user} thumbnail={`${item.thumbnail}`}
                        title={item.judul} subject={item.mapel} photoProfile={item.photo_profile} />
                    ))}
                  </div>
                )}
              </InfiniteScroll>
            </div>
          </div>
        </div>
        {isShowDetailVideo && (
          <CModalContainer>
            <CDetailMaterialContent onClose={() => this.setState({ isShowDetailVideo: false })} data={detailVideoData} />
          </CModalContainer>
        )}
        {/* <ConstructionPage /> */}
      </View>
    )
  }
}

const CSubjectContentLoading = () => (
    <div className='mt-3 space-y-0 grid md:grid md:grid-cols-2 lg:grid lg:grid-cols-4 md:gap-x-6 lg:gap-x-3'>
      {Array(8).fill('').map((_,i) => (
        <View key={i} style={{ marginBottom:10 }}>
          <CLoadingSubject />
        </View>
      ))}
    </div>
)

export default Material

const tailStyles = {
  container: tailwind('relative w-full h-full'),
}

const styles = StyleSheet.create({

})