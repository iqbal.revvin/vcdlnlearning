import React from 'react'
import { View } from 'react-native-web'
import CSkeleton from '../../../components/Loaders/CSkeleton'

const CLoadingSubject = () => {
    return (
        <View style={{ alignItems:'center' }}>
            <CSkeleton height={200} width={300} />
            <View style={{ flexDirection:'row', marginTop: 10, justifyContent: 'space-between' }}>
                <View>
                    <CSkeleton height={50} width={50} radius={50} />
                </View>
                <View>
                    <CSkeleton height={20} width={200} />
                    <View style={{ height:5 }} />
                    <CSkeleton height={20} width={225} />
                    <View style={{ height:5 }} />
                    <CSkeleton height={20} width={150} />
                </View>
            </View>
        </View>
    )
}

export default React.memo(CLoadingSubject)