/*eslint-disable*/
import React from "react";
// components

export default function CNavbar() {
    const [navbarOpen, setNavbarOpen] = React.useState(false);
    return (
        <>
            <nav className="top-100 fixed z-50 w-full flex flex-wrap items-center justify-between px-2 py-3 navbar-expand-lg bg-white shadow-sm">
                <div className="container px-4 mx-auto flex flex-wrap items-center justify-between">
                    <div className="w-full relative flex justify-between w-auto :static md:block lg:justify-start md:justify-start hidden">
                        <a href='/' className="text-blueGray-700 text-sm font-bold leading-relaxed inline-block mr-4 py-2 whitespace-nowrap uppercase text-center">
                            VCDLNLearning
                        </a>
                    </div>
                    <div className="lg:flex md:flex flex-grow items-center bg-white lg:bg-opacity-0 lg:shadow-none" id="example-navbar-warning">

                        <ul className="flex flex-row list-none ml-auto  bg-red-0">
                            {/* <div className='flex flex-row w-auto min-w-full'>
                                <div className='absolute left-[750px] right-0 top-0 bottom-0'>
                                    <i className='fas fa-search' />
                                </div>
                                <input type='text' placeholder='Search' className='min-w-full w-96' />
                            </div> */}
                            <div className="bg-white shadow flex w-auto min-w-full p-2">
                                <span className="w-auto flex justify-end items-center text-gray-500 p-2">
                                    <i className='fas fa-search' />        
                                </span>
                                <input className="md:w-96 w-full rounded p-2 focus:outline-none border-0" type="text" placeholder="Cari Materi"/>
                                <button className="bg-red-600 hover:bg-red-700 rounded text-white p-2 px-8">
                                    <p className="font-semibold text-xs">Cari</p>
                                </button>
                            </div>
                        </ul>
                    </div>
                </div>
            </nav>
        </>
    );
}
