import React from 'react'
import TextTruncate from 'react-text-truncate'
import CPhotoNull from '../../../components/Photos/CPhotoNull'
import CText from '../../../components/Text/CText'

const CContentItem = ({ onClick, id, thumbnail, creator, title, subject, photoProfile }) => {
    return (
      <div className="group relative" onClick={onClick}>
        <div className="relative w-full h-[200px] sm:w-[300px] sm:h-[200px] bg-white rounded-lg overflow-hidden mt-5 shadow-md">
          <a href='#' className='absolute text-3xl z-10 top-2 bottom-0 left-65 right-2'>
            <i className='fas fa-play-circle text-red-700 hover:text-red-800 shadow-lg rounded-full border-4 bg-white hover:border-gray-300' />
          </a>
          <img src={thumbnail} alt={title} className="w-full object-fill opacity-90 " />
        </div>
        <div className='flex flex-row mt-2'>
          <div className='m-2 flex-none'>
            {/* <img src={`https://picsum.photos/id/${id * Math.floor(Math.random() * 20)}/200/200`} className='w-12 h-12 rounded-full' /> */}
            {photoProfile && (
              <img src={photoProfile} className='w-12 h-12 rounded-full' />
            )}
            {!photoProfile && (
              <CPhotoNull name={creator} />
            )}
          </div>
          <div className='m-2 w-full'>
            <h3 className="mt-1 text-sm">
              <a href='#' style={{ color: '#030303' }}>
                <span className="absolute inset-0" />
                {creator}
              </a>
            </h3>
            <div className="text-base font-semibold" style={{ color: '#030303' }}>
              <TextTruncate line={1} element="p" truncateText="…" text={title} />
            </div>
            <h6>
              <CText italic style={{ color: '#475569' }}>
                <TextTruncate line={1} element="p" truncateText="…" text={subject}/>
              </CText>
            </h6>
          </div>
        </div>
      </div>
    )
  }

const equal = (prevProps, nextProps) => {
    return JSON.stringify(prevProps) === JSON.stringify(nextProps)
}

export default React.memo(CContentItem, equal)