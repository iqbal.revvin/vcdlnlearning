import React from 'react'
import 'video-react/dist/video-react.css'; // import css
import { StyleSheet, View, TouchableOpacity } from 'react-native-web'
import tailwind from "tailwind-rn";
import CText from '../../../components/Text/CText'
import {
    Player, ControlBar, ReplayControl, ForwardControl, CurrentTimeDisplay,
    TimeDivider, PlaybackRateMenuButton, VolumeMenuButton
} from 'video-react';
import ReactPlayer from 'react-player';

const CDetailMaterialContent = ({onClose, data}) => {
    return (
        <View style={styles.container}>
            <View style={styles.playerContainer}>
                <Player poster={data.thumbnail} preload='auto'>
                    <source src={data.file_video.slice(6)} />
                    {/* <source src='http://peach.themazzone.com/durian/movies/sintel-1024-surround.mp4' /> */}
                    <ControlBar>
                        <ReplayControl seconds={10} order={1.1} />
                        <ForwardControl seconds={30} order={1.2} />
                        <CurrentTimeDisplay order={4.1} />
                        <TimeDivider order={4.2} />
                        <PlaybackRateMenuButton rates={[5, 2, 1, 0.5, 0.1]} order={7.1} />
                        <VolumeMenuButton />
                    </ControlBar>
                </Player>
                <button className='absolute top-0 bottom-5 -right-5 left-50 text-xl w-20 h-12 opacity-20 hover:opacity-75' onClick={onClose}>
                    <i className='fas fa-times p-1 text-red-500 hover:text-red-800 shadow-lg rounded-full border-4 bg-gray-50 hover:border-gray-300' />
                </button>
            </View>

        </View>
    )
}

export default CDetailMaterialContent

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        position:'fixed',
        width: '100%',
        height: '100%',
        alignSelf: 'center',
        top:0,
        bottom:0
    },
    playerContainer:{
     
    }
})