import React, { useState, useEffect } from 'react'
import { StyleSheet, View, ScrollView, TouchableOpacity } from 'react-native-web'
import Select from 'react-select'
import tailwind from 'tailwind-rn'
import CText from '../../../components/Text/CText'
import Skeleton from 'react-loading-skeleton';
import CSkeleton from '../../../components/Loaders/CSkeleton'
import { getCategoryFilterList, getCategoryList, getMapelFilterList } from '../action'

const CFilterFormContent = ({ subjectsSelect }) => {
    const [loading, setLoading] = useState(false)
    const [chipData, setChipData] = useState([])

    const [categorySelect, setCategorySelect] = useState([
        { value: '', label: 'Pilih terlebih dahulu jenjang' }
    ])

    const [mapelSelect, setMapelSelect] = useState([
        { value: '', label: 'Pilih terlebih dahulu kategori' }
    ])
    
    useEffect(() => {
        handleGetCategoryList()
        return () => {
            null
        }
    }, [])    

    const handleGetCategoryList = async () => {
        setLoading(true)
        try {
            const response = await getCategoryList()
            if(response?.data?.success){
                const responseData = response.data.output_data
                const newData = responseData.map((item => ({id:item.id, name:item.nama, active:false})))
                setChipData([{id: 0, name:'Semua', active:true}, ...newData])
                setLoading(false)
            }else{
                console.log(response)
            }
        } catch (error) {
            console.log('Materi.CFilterFormContent@handleGetCategoryList', error)
        }
    }

    const handleClickChip = (id) => {
        setChipData(prevState => prevState.map(item => item.id == id ? {...item, active:true} : {...item, active:false}))
    }

    const handleOnClickJenjang = async (value) => {
        try {
            const response = await getCategoryFilterList(value)
            if(response?.data?.success){
                const responseData = response.data.output_data
                const newData = responseData.map((item => ({value:item.id, label:item.nama })))
                setCategorySelect(newData)
                // console.log(newData)
            }else{
                alert('Terjadi kesalahan saat mengambil data, coba reload browser anda')
            }
        } catch (error) {
            console.log('Materi.CFilterFormContent@handleOnClickJenjang', error)
        }
    }

    const handleOnClickKategori = async e => {
        try {
            const response = await getMapelFilterList(e.value)
            if(response?.data?.success){
                const responseData = response.data.output_data
                const newData = responseData.map((item => ({value:item.id, label:item.nama})))
                setMapelSelect(newData)
                // console.log(newData)
            }else{
                alert('Terjadi kesalahan saat mengambil data, coba reload browser anda')
            }
        } catch (error) {
            console.log('Materi.CFilterFormContent@handleOnClickKategori', error)
        }
    }

    const handleClickMapel = e => {
        
    }

    return (
        <div className='flex flex-col z-20'>
            <View style={{ bottom:15 }}>
                {/* <CText>{JSON.stringify(chipData)}</CText> */}
                {loading && (
                    <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                        {Array(12).fill('').map((_,i) => (
                            <CSkeleton key={i} width={75} height={25} color='lightgray' style={{marginRight:15}} />
                        ))}
                    </ScrollView>
                )}
                {!loading && (
                    <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                        {chipData.map((item, i) => {
                            const activeChip = item.active ? {backgroundColor:'black'}:null
                            const activeText = item.active ? {color:'white'} : {color:'black'}
                            return(
                                <TouchableOpacity key={i} style={[styles.chipItemContainer, activeChip]} onPress={() => handleClickChip(item.id)}>
                                    <CText style={{ fontSize: 12, ...activeText }}>{item.name}</CText>
                                </TouchableOpacity>
                            )
                        })}
                    </ScrollView>
                )}
            </View>
            <div className='flex flex-col md:flex-row md:gap-x-3'>
                <div className="relative w-full mb-3">
                    <label className={tailStyles.labelSelectSection} htmlFor='jenjang'>
                        Jenjang
                    </label>
                    <select className={tailStyles.fieldSelectFilter} onChange={(e) => handleOnClickJenjang(e.target.value)}>
                        <option value='' className='text-blueGray-500'>Pilih Jenjang Materi</option>
                        <option value='Semua'>Semua</option>
                        <option value='SD'>SD</option>
                        <option value='SMP'>SMP</option>
                        <option value='SMA'>SMA</option>
                        <option value='SMK'>SMK</option>
                    </select>
                </div>
                <div className='relative w-full mb-3 z-30'>
                    <label className={tailStyles.labelSelectSection} htmlFor='kategori'>
                        Kategori
                    </label>
                    <Select className={tailStyles.fieldSelectFilter} defaultValue={categorySelect[0]} isDisabled={false}
                        isLoading={false} isClearable={true} isSearchable={true} options={categorySelect} onChange={handleOnClickKategori}
                    />
                </div>
                <div className='relative w-full mb-3 z-20'>
                    <label className={tailStyles.labelSelectSection} htmlFor='matpel'>
                        Mata Pelajaran
                    </label>
                    <Select className={tailStyles.fieldSelectFilter} defaultValue={mapelSelect[0]} isDisabled={false}
                        isLoading={false} isClearable={true} isSearchable={true} options={mapelSelect}
                    />
                </div>
            </div>
        </div>
    )
}

const equal = (prevProps, nextProps) => {
    return JSON.stringify(prevProps) === JSON.stringify(nextProps)
}

export default React.memo(CFilterFormContent, equal)

const tailStyles = {
    filterFormContainer: tailwind('flex flex-col'),
    labelSelectSection: 'block uppercase text-blueGray-600 text-xs font-bold mb-2',
    fieldSelectFilter: 'border-0 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150',
}

const styles = StyleSheet.create({
    chipFilterContainer: {

    },
    chipItemContainer: {
        backgroundColor: 'ghostwhite',
        padding: 5,
        borderWidth: 2,
        borderColor: 'darkgray',
        borderRadius: 25,
        marginRight: 10

    }
})