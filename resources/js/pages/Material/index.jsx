import ReactDOM from 'react-dom'
import Material from './Material'

if(document.getElementById('material-render')){
    ReactDOM.render(<Material />, document.getElementById('material-render'))
}