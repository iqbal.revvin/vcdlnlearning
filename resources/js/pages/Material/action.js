import API, { CATEGORY, CATEGORY_FILTER_LIST, MAPEL_FILTER_LIST, SUBJECT_LIST } from "../../services/api/ApiContainer"

export const getCategoryList = async () => {
    try {
        const request = await API.get(`${CATEGORY}`)
        return request
    } catch (error) {
        console.log('Material.action@getCategoryList', error)
    }
}

export const getCategoryFilterList = async (keyword) => {
    try {
        const request = await API.get(`${CATEGORY_FILTER_LIST}?jenjang=${keyword}`)
        return request
    } catch (error) {
        console.log('Material.action@getCategoryFilterList', error)
    }
}

export const getMapelFilterList = async (kategoriID) => {
    try {
        const request = await API.get(`${MAPEL_FILTER_LIST}?kategoriId=${kategoriID}`)
        return request
    } catch (error) {
        console.log('Material.action@getMapelFilterList', error)
    }
}

export const getSubjectList = async () => {
    try {
        const request = await API.get(`${SUBJECT_LIST}`)
        return request
    } catch (error) {
        console.log('Material.action@getSubjectList', error)
    }
}