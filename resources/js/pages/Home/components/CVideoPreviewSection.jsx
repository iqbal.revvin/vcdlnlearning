import React, {Fragment} from 'react'
import CButtonRed from '../../../components/Buttons/CButtonRed'

const CVideoPreviewSection = () => {
    const exampleVidData = ['fFlEBTg3Yuc', 'hGDec-Jpm4E', 'AjG5FLTSR5Q', 'tjY-c9aMvj0']
    return(
        <Fragment>
            <div className='container mx-auto px-4'>
                <div className='flex justify-center text-center'>
                    <div className='w-full lg:w-8/12 px-4'>
                        <h2 className='text-4xl font-semibold'>Examples we present</h2>
                        <p className='text-lg leading-relaxed m-4 text-blueGray-500'>
                            The learning content that we present is provided specifically for all levels of school and the general public, 
                            so that students and the wider community can enjoy our content comfortably.
                        </p>
                    </div>
                </div>
                <div className='flex flex-wrap'>
                    {exampleVidData.map((item,i) => (
                        <div key={i} className='w-full md:w-6/12 lg:w-6/12 mt-10'>
                            <div className='px-3'>
                                <div className=''>
                                    <iframe src={`https://www.youtube.com/embed/${item}`}
                                        className='shadow-lg rounded-md object-contain w-full md:h-auto lg:h-[320px] h-[318px]'
                                        title="YouTube video player" 
                                        frameborder="0" 
                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" 
                                        allowfullscreen/>
                                </div>
                            </div>
                        </div>
                    ))}
                </div>
                <div className='text-center m-20 animate-bounce'>
                    <CButtonRed href='/materi' title='View More Content'/>
                </div>
            </div>
        </Fragment>
    )
}

export default CVideoPreviewSection