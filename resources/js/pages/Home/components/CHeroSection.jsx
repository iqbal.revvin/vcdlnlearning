import React, { Fragment } from 'react';

const CHeroSection = () => {
    return (
        <Fragment>
            <div className="absolute top-0 w-full h-full bg-center bg-cover" style={{ backgroundImage: "url('./homepage-bg-min.jpg')" }}>
                <span id="blackOverlay" className="w-full h-full absolute opacity-75 bg-black"></span>
            </div>
            <div className="container relative mx-auto">
                <div className="items-center flex flex-wrap">
                    <div className="w-full lg:w-6/12 px-4 ml-auto mr-auto text-center">
                        <div className="py-5">
                            <h1 className="text-white font-semibold text-5xl">
                                Virtual Community<br/> 
                                Digital Learning Nusantara
                            </h1>
                            <p className="mt-4 text-lg text-blueGray-200">
                            Find and share learning digital content for all levels of school in Indonesia, 
                            and reach content easily on various platforms at your fingertips
                            </p>
                        </div>
                        <div className='mt-5'>
                            <a href='/materi' className='py-3 px-5 bg-red-600 text-center shadow-md active:bg-sky-700 hover:bg-sky-600 hover:shadow-lg text-white font-bold rounded-lg uppercase ease-linear transition-all duration-150'>Access Learning</a>
                        </div>
                    </div>
                </div>
            </div>
            <div style={{ transform: "translateZ(0)" }} className="top-auto bottom-0 left-0 right-0 w-full absolute pointer-events-none overflow-hidden h-70-px">
                <svg className="absolute bottom-0 overflow-hidden" xmlns="http://www.w3.org/2000/svg" 
                    version="1.1" viewBox="0 0 2560 100" x="0" y="0" preserveAspectRatio="none"
                >
                    <polygon className="text-blueGray-200 fill-current" points="2560 0 2560 100 0 100"></polygon>
                </svg>
            </div>
        </Fragment>
    );
}



export default CHeroSection;
