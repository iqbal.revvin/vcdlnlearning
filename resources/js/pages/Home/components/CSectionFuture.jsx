import React, { Fragment } from 'react';

const CSectionFuture = () => {
    return (
        <Fragment>
            <div className="flex flex-wrap">
                <div className="lg:pt-12 pt-6 w-full md:w-4/12 px-4 text-center">
                    <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg py-5">
                        <div className="px-4 py-5 flex-auto">
                            <a href='https://www.vcdln-tvupi.com/' target='_blank'>
                                <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-red-600">
                                    <i className="fas fa-home"></i>
                                </div>
                                <h6 className="text-xl font-semibold">VCDLN TVUPI</h6>
                                <p className="mt-2 mb-4 text-blueGray-500">
                                Virtual Community Digital Learning Network in Order to Affirm the Design for PJJ, Blended and Mobile e-learning services
                                </p>
                            </a>
                        </div>
                    </div>
                </div>

                <div className="w-full md:w-4/12 px-4 text-center">
                    <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg  py-5">
                        <div className="px-4 py-5 flex-auto">
                            <a href='http://tv.upi.edu/' target='_blank'>
                                <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-sky-400">
                                    <i className="fas fa-home"></i>
                                </div>
                                <h6 className="text-xl font-semibold">TVUPI EDU</h6>
                                <p className="mt-2 mb-4 text-blueGray-500">
                                Virtual TV service from Universitas Pendidikan Indonesia in order to improve effective audio-visual-based literacy.
                                </p>
                            </a>
                        </div>
                    </div>
                </div>

                <div className="pt-6 w-full md:w-4/12 px-4 text-center">
                    <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg py-5">
                        <div className="px-4 py-5 flex-auto">
                            <a href='https://www.upi.edu/' target='_blank'>
                                <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-black">
                                    <i className="fas fa-home"></i>
                                </div>
                                <h6 className="text-xl font-semibold">UPI</h6>
                                <p className="mt-2 mb-4 text-blueGray-500">
                                A comprehensive university that currently offers a variety of disciplines, especially education.
                                </p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div className="flex flex-wrap items-center mt-16">
                <div className="w-full md:w-5/12 px-4 mr-auto ml-auto">
                    <div className="text-blueGray-500 p-3 text-center inline-flex items-center justify-center w-16 h-16 mb-6 shadow-lg rounded-full bg-white">
                        <i className="fas fa-user-friends text-xl"></i>
                    </div>
                    <h3 className="text-3xl mb-2 font-semibold leading-normal">
                        Working with us is a pleasure
                    </h3>
                    <p className="text-lg font-light leading-relaxed mt-4 mb-4 text-blueGray-600">
                    VCDLN LEARNING Is a Virtual Community Digital Learning Network in order to emphasize the design for PJJ and Database services,
                         Blended and Mobile e-learning that is easy, cheap, broad and reaches all corners of the archipelago through multi-platform mobile telecommunications, such as android, IOS, Streaming, LCJ, youtube, and satellite TV.
                    </p>

                    {/* <a to="/" className="font-bold text-blueGray-700 mt-8">
                        Check Notus React!
                    </a> */}
                </div>

                <div className="w-full md:w-4/12 px-4 mr-auto ml-auto">
                    <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-lg rounded-lg bg-sky-500">
                        <img
                            alt="..."
                            src="https://images.unsplash.com/photo-1522202176988-66273c2fd55f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1051&q=80"
                            className="w-full align-middle rounded-t-lg"
                        />
                        <blockquote className="relative p-8 mb-4">
                            <svg
                                preserveAspectRatio="none"
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 583 95"
                                className="absolute left-0 w-full block h-95-px -top-94-px"
                            >
                                <polygon
                                    points="-30,95 583,95 583,65"
                                    className="text-sky-500 fill-current"
                                ></polygon>
                            </svg>
                            <h4 className="text-xl font-bold text-white">
                                TWholehearted Service
                            </h4>
                            <p className="text-md font-light mt-2 text-white">
                            we are very excited in implementing and making this application.
                            </p>
                        </blockquote>
                    </div>
                </div>
            </div>
        </Fragment>
    );
}

export default CSectionFuture;
