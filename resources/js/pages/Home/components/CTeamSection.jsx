import React, { Fragment, useState } from 'react';
import {deniPhoto, dianPhoto, andriPhoto, fajarPhoto, iqbalPhoto} from '../../../assets/images/persons';

const CTeamSection = () => {
    const [team, setTeam] = useState([
        { id: 1, name: 'Prof. Dr. Deni D., M.Si', position: 'Bok Lead Reseacher', facebook:'https://facebook.com', instagram:'https://instagram.com', website:'https://google.com', src:deniPhoto },
        { id: 2, name: 'Dian Rahadian, M.Pd', position: 'Reseacher Member 1', facebook:'https://facebook.com', instagram:'https://instagram.com', website:'https://google.com', src:dianPhoto },
        { id: 2, name: 'Andri Suryadi, M.Kom', position: 'Reseacher Member 2', facebook:'https://facebook.com', instagram:'https://instagram.com', website:'https://google.com', src:andriPhoto },
        { id: 3, name: 'Fajar Maulanan, S.Pd', position: 'Laboran Specialist', facebooK:'https://facebook.com', instagram:'https://instagram.com', website:'https://google.com', src:fajarPhoto }
        
    ])
    return (
        <Fragment>
            <div className="container mx-auto px-4">
                <div className="flex flex-wrap justify-center text-center mb-24">
                    <div className="w-full lg:w-6/12 px-4">
                        <h2 className="text-4xl font-semibold">Here are our heroes</h2>
                        <p className="text-lg leading-relaxed m-4 text-blueGray-500">
                            The development of VCDLNLeraning involves several parties who concentrate on several fields including professors, lecturers, researchers and software developers.
                        </p>
                    </div>
                </div>
                <div className="flex flex-wrap">
                    {team.map((item, i) => (
                        <div key={i} className="w-full md:w-6/12 lg:w-3/12 lg:mb-0 mb-12 px-4">
                            <div className="px-6" >
                                <img alt="The Team VCDLNLearning" src={item.src}
                                    className="shadow-lg rounded-full mx-auto h-28 w-28 object-contain"
                                />
                                <div className="pt-6 text-center">
                                    <h5 className="text-xl font-bold">{item.name}</h5>
                                    <p className="mt-1 text-sm text-blueGray-400 uppercase font-semibold">
                                        {item.position}
                                    </p>
                                    <div className="mt-6">
                                        <button type="button" className="bg-sky-600 text-white w-8 h-8 rounded-full outline-none focus:outline-none mr-1 mb-1">
                                            <i className="fab fa-facebook-f"></i>
                                        </button>
                                        <button type="button" className="bg-pink-500 text-white w-8 h-8 rounded-full outline-none focus:outline-none mr-1 mb-1">
                                            <i className="fab fa-instagram"></i>
                                        </button>
                                        <button type="button" className="bg-gray-500 text-white w-8 h-8 rounded-full outline-none focus:outline-none mr-1 mb-1">
                                            <i className="fab fa-google"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        </Fragment>
    );
}

export default CTeamSection;
