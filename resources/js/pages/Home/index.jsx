import ReactDOM from 'react-dom'
import Homepage from './Homepage'

if(document.getElementById('homepage')) {
    ReactDOM.render(<Homepage />, document.getElementById('homepage'))
}