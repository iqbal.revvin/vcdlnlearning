import React, { Component } from 'react';

class ImportDataScreen extends Component {
    state = {
        count: 0
    }
    render() {
        return (
            <div className=''>
                <div className=' mx-auto my-4'>
                <div className='antialiased flex flex-row justify-center'>
                    <div className='w-full laptop:w-4/12'>
                        <div className='bg-white rounded py-4 shadow-xl text-center px-2 '>
                            <h2 className='font-bold '>File Import Component</h2>
                            <p>You clicked {this.state.count} times</p>
                        </div>
                        <div className='text-center py-3'>
                            <button className='bg-blue-500 hover:bg-blue-600 active:opacity-50 px-5 py-2 rounded-md text-white focus:outline-none shadow-lg' onClick={() => this.setState({count: this.state.count+1})}>Test Click</button>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        );
    }
}


export default ImportDataScreen;

