import ReactDOM from 'react-dom'
import ImportDataScreen from './ImportDataScreen'

if(document.getElementById('ImportDataScreen')) {
    ReactDOM.render(<ImportDataScreen />, document.getElementById('ImportDataScreen'))
}