import React from 'react'
import {StyleSheet,View} from 'react-native-web'
import CText from '../Text/CText'

const CPhotoNull = ({name}) => {
    const colors = ['#a2d2ff', '#ee9b00', '#e63946', '#d4a373', '#014f86', '#06d6a0', '#83c5be', '#e29578']
    const random = colors[Math.floor(Math.random()*colors.length)]
    const styles = style(random)
    return(
        <View style={styles.container}>
            <View>
            <CText size={200} style={{ fontSize:22 }} bold>{name.charAt(0).toUpperCase()}</CText>
            </View>
            
        </View>
    )
}

const style = (random) => StyleSheet.create({
    container: {
        backgroundColor:random, 
        width:45, 
        height:45, 
        borderRadius: 25,
        alignItems: 'center',
        justifyContent: 'center'
    }
})

export default React.memo(CPhotoNull)