import React from 'react'
import {Text} from 'react-native-web'

const CText = ({children, style, bold, italic}) => {
    const fontWeight = bold?'bold':'normal'
    const fontStyle = italic?'italic':'normal'
    return(
        <Text style={{ fontWeight:fontWeight, fontStyle:fontStyle, ...style}}>
            {children}
        </Text>
    )
}

const compare = (prevProps, nextProps) => {
    return JSON.stringify(prevProps) === JSON.stringify(nextProps)
}

export default React.memo(CText, compare)