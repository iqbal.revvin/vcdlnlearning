import React, {Fragment} from 'react'

const CButtonRed = ({title, href, onClick}) => {
    return(
        <div>
            <a href={href||'#'} onClick={onClick} 
                className='py-3 px-5 bg-red-600 text-center shadow-md active:bg-sky-700 hover:bg-sky-600 hover:shadow-lg text-white font-bold rounded-lg uppercase ease-linear transition-all duration-150'>
                    {title}
                </a>
        </div>
    )
}

CButtonRed.defaultProps = {
    title: 'Button Red Title'
}

export default CButtonRed