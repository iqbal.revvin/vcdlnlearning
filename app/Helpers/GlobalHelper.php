<?php
    namespace App\Helpers;

    class GlobalHelper {
        public static function createResponse($success, $message, $data=null, $additional_data=null){
            $api_key = env('MIX_API_KEY');
            $response = [
                'success' => $success,
                'message' => $message,
            ];
            if($data){
                $response += [
                    'output_data' => $data
                ];
            }
            if($additional_data){
                $response += [
                    'additional_data' => $additional_data
                ];
            }
            if(request()->header('x-api-key') == $api_key){
                return response()->json($response);
            }else{
                return response()->json([
                    'success' => false,
                    'message' => 'Api Key not valid',
                ], 403);
            }
        }
    }