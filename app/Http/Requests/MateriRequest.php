<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\Materi;
use App\Rules\YoutubeUrl;
use Illuminate\Foundation\Http\FormRequest;

class MateriRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->get('id') ?? request()->route('id');
        if($id){
            $materi = Materi::find($id);
            return [
                'jenjang' => 'required',
                'mapel_id' => 'required|integer',
                'kategori_id' => 'required|integer',
                'judul' => 'required|max:200',
                'file_video' =>['mimetypes:video/avi,video/mp4,video/mpeg,video/quicktime',!$materi->file_video?'required_if:url_video_youtube,""':false,'max:250000'],
                'url_video_youtube' => [!$materi->file_video?'required_if:file_video,""':false, 'nullable', new YoutubeUrl],
                'thumbnail' => ['required_if:url_video_youtube,""'],
                'file_rpp' => 'mimes:pdf,docx,doc,xls,xlsx|max:10000',
                'flow_diagram' => 'mimes:pdf,docx,doc,xls,xlsx|max:10000',
                'story_board' => 'mimes:pdf,docx,doc,xls,xlsx|max:10000',
                'deskripsi' => 'required|string|max:5000'
            ];
        }else{
            return [
                'jenjang' => 'required',
                'mapel_id' => 'required|integer',
                'kategori_id' => 'required|integer',
                'judul' => 'required|max:200',
                'file_video' => 'mimetypes:video/avi,video/mp4,video/mpeg,video/quicktime|required_if:url_video_youtube,""|max:250000',
                'url_video_youtube' => ['required_if:file_video,""', 'nullable', new YoutubeUrl],
                'thumbnail' => ['required_if:url_video_youtube,""'],
                'file_rpp' => 'mimes:pdf,docx,doc,xls,xlsx|max:10000',
                'flow_diagram' => 'mimes:pdf,docx,doc,xls,xlsx|max:10000',
                'story_board' => 'mimes:pdf,docx,doc,xls,xlsx|max:10000',
                'deskripsi' => 'required|string|max:5000'
            ];
        }
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'file_video.required_if' => 'File video wajib di unggah jika url youtube tidak di isi!',
            'url_video_youtube.required_if' => 'Url youtube wajib di isi jika file video tidak di unggah!',
            'thumbnail.required_if' => 'Thumbnail wajib di isi jika tidak menggunakan video dari youtube!'
        ];
    }
}
