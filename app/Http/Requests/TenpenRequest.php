<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class TenpenRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(backpack_user()->hasRole('Developer') || backpack_user()->hasRole('Admin')) {
            return [
                'sekolah_id' => 'required',
                'user_id' => 'unique:users,email',
                'nik' => 'nullable|max:16',
                'nuptk' => 'nullable|max:16',
                'nip' => 'nullable',
                'niy_nigk' => 'nullable',
                'tmt' => 'date',
                'tst' => 'nullable|date',
                'nama_lengkap' => 'required|max:150',
                'jenis_kelamin' => 'required',
                'tempat_lahir' => 'nullable',
                'tanggal_lahir' => 'nullable|date',
                'agama' => 'required',
                'pendidikan' => 'nullable',
                'kewarganegaraan' => 'nullable'
            ];
        }else{
            return [
                'nik' => 'nullable|max:16',
                'nuptk' => 'nullable|max:16',
                'nip' => 'nullable',
                'niy_nigk' => 'nullable',
                'tmt' => 'date',
                'tst' => 'nullable|date',
                'nama_lengkap' => 'required|max:150',
                'jenis_kelamin' => 'required',
                'tempat_lahir' => 'nullable',
                'tanggal_lahir' => 'nullable|date',
                'agama' => 'required',
                'pendidikan' => 'nullable',
                'kewarganegaraan' => 'nullable',
                'foto' => 'required'
            ];
        }
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
