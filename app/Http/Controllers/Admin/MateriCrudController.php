<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\MateriRequest;
use App\Models\Mapel;
use App\Models\User;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Str;
use App\Models\Materi;

/**
 * Class MateriCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class MateriCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation { store as traitStore; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\FetchOperation;
    use \App\Http\Controllers\Admin\Operations\ChangeStatusOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Materi::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/materi');
        CRUD::setEntityNameStrings('Materi', 'materi');
        if(!backpack_user()->hasRole('Developer') && !backpack_user()->hasRole('Admin')) {
            $this->crud->denyAccess('changestatus');
        }
    }   

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::setFromDb(); // columns

        //Remove Column
        $this->crud->removeColumn('slug');
        $this->crud->removeColumn('deskripsi');
        $this->crud->removeColumn('tindakan');
        $this->crud->removeColumn('keterangan');

        if(!backpack_user()->hasRole('Developer') && !backpack_user()->hasRole('Admin')){
            $this->crud->addClause('where', 'user_id', backpack_user()->id);
        }

        // $this->crud->column('user_id')->type('relationship')->attribute('user');
        // $this->crud->column('mapel_id')->type('relationship')->attribute('mapel');
        // $this->crud->column('kategori_id')->type('relationship')->attribute('kategori');

        if(backpack_user()->hasRole('Developer') || backpack_user()->hasRole('Admin')) {
            $this->crud->addFilter([
                'name'  => 'status',
                'type'  => 'dropdown',
                'label' => 'Status'
            ], [
                'Aktif' => 'Aktif',
                'NonAktif' => 'NonAktif',
                'Pending' => 'Pending',
            ], function($value) { // if the filter is active
                $this->crud->addClause('where', 'status', $value);
            });
            $this->crud->addFilter([
                'name' => 'select2',
                'type' => 'select2',
                'label' => 'Pengguna'
            ], function(){
                return User::NotDeveloperUser()->lazyById()->pluck('name', 'id')->toArray();
            }, function($value){
                $this->crud->addClause('where', 'user_id', $value);
            });
        }
        
        if(!backpack_user()->hasRole('Developer') && !backpack_user()->hasRole('Admin')) {
            $this->crud->removeColumn('user_id');
        }


        $this->crud->modifyColumn('user_id', [
            'name'         => 'user', // name of relationship method in the model
            'type'         => 'relationship',
            'label'        => 'User Account', // Table column heading
            'attribute'    => 'email'
        ]);

        $this->crud->modifyColumn('mapel_id', [
            'name'         => 'mapel', // name of relationship method in the model
            'type'         => 'relationship',
            'label'        => 'Mata Pelajaran', // Table column heading
            'attribute'    => 'nama'
        ]);

        $this->crud->modifyColumn('kategori_id', [
            'name'         => 'kategori', // name of relationship method in the model
            'type'         => 'relationship',
            'label'        => 'Kategori', // Table column heading
            'attribute'    => 'nama'
        ]);

        $this->crud->modifyColumn('file_video', [
            'type'         => 'closure',
            'label'        => 'File Video', // Table column heading
            'function' => function ($entry) {
                if ($entry->file_video != null) {
                    return 'Lihat Video';
                } else {
                    return 'Tidak diunggah';
                }
            },
            'wrapper' => [
                'element' => 'a',
                'href' => function ($crud, $column, $entry, $related_key) {
                    $public_destination_path = Str::replaceFirst('public/', '', $entry->file_video);
                    return asset($public_destination_path);
                },
                'target' => '_blank',
                'class' => function ($crud, $column, $entry, $related_key) {
                    if ($entry->file_video != null) {
                        return 'badge badge-success';
                    }
                    return 'badge badge-default';
                },
            ],
        ]);

        $this->crud->modifyColumn('url_video_youtube', [
            'type'         => 'closure',
            'label'        => 'URL Youtube', // Table column heading
            'function' => function ($entry) {
                if ($entry->url_video_youtube != null) {
                   return 'Lihat Video';
                } else {
                    return 'Tidak disisipkan';
                }
            },
            'wrapper' => [
                'element' => 'a',
                'href' => function ($crud, $column, $entry, $related_key) {
                    return $entry->url_video_youtube;
                },
                'target' => '_blank',
                'class' => function ($crud, $column, $entry, $related_key) {
                    if ($entry->url_video_youtube != null) {
                        return 'badge badge-success';
                    }
                    return 'badge badge-default';
                },
            ],
        ]);

        $this->crud->modifyColumn('thumbnail', [
            'label'         => "Thumbnal",
            'name'          => "thumbnail",
            'type'          => 'image',
        ]);

        $this->crud->modifyColumn('file_rpp', [
            'type'         => 'closure',
            'label'        => 'File RPP', // Table column heading
            'function' => function ($entry) {
                if ($entry->file_rpp != null) {
                    return 'Download Dokumen';
                } else {
                    return 'Tidak diunggah';
                }
            },
            'wrapper' => [
                'element' => 'a',
                'href' => function ($crud, $column, $entry, $related_key) {
                    $public_destination_path = Str::replaceFirst('public/', '', $entry->file_rpp);
                    return asset($public_destination_path);
                },
                'target' => '_blank',
                'class' => function ($crud, $column, $entry, $related_key) {
                    if ($entry->file_rpp != null) {
                        return 'badge badge-info';
                    }
                    return 'badge badge-default';
                },
            ],
        ]);

        $this->crud->modifyColumn('flow_diagram', [
            'type'         => 'closure',
            'label'        => 'Flow Diagram', // Table column heading
            'function' => function ($entry) {
                if ($entry->flow_diagram != null) {
                    return 'Download Dokumen';
                } else {
                    return 'Tidak diunggah';
                }
            },
            'wrapper' => [
                'element' => 'a',
                'href' => function ($crud, $column, $entry, $related_key) {
                    $public_destination_path = Str::replaceFirst('public/', '', $entry->flow_diagram);
                    return asset($public_destination_path);
                },
                'target' => '_blank',
                'class' => function ($crud, $column, $entry, $related_key) {
                    if ($entry->flow_diagram != null) {
                        return 'badge badge-info';
                    }
                    return 'badge badge-default';
                },
            ],
        ]);

        $this->crud->modifyColumn('story_board', [
            'type'         => 'closure',
            'label'        => 'Story Board', // Table column heading
            'function' => function ($entry) {
                if ($entry->story_board != null) {
                    return 'Download Dokumen';
                } else {
                    return 'Tidak diunggah';
                }
            },
            'wrapper' => [
                'element' => 'a',
                'href' => function ($crud, $column, $entry, $related_key) {
                    $public_destination_path = Str::replaceFirst('public/', '', $entry->story_board);
                    return asset($public_destination_path);
                },
                'target' => '_blank',
                'class' => function ($crud, $column, $entry, $related_key) {
                    if ($entry->story_board != null) {
                        return 'badge badge-info';
                    }
                    return 'badge badge-default';
                },
            ],
        ]);

        $this->crud->modifyColumn('status', [
            'type'         => 'text',
            'label'        => 'Status', // Table column heading
            'wrapper' => [
                'element' => 'span',
                'class' => function ($crud, $column, $entry, $related_key) {
                    if ($entry->status == 'Aktif') {
                        return 'badge badge-success';
                    }else if($entry->status == 'NonAktif'){
                        return 'badge badge-danger';
                    }
                    return 'badge badge-warning';
                },
            ],
        ]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(MateriRequest::class);

        CRUD::setFromDb(); // fields
        
        $this->crud->removeField('slug');

        if (backpack_user()->hasRole('Developer') || backpack_user()->hasRole('Admin') ) {
            CRUD::modifyField('user_id', [
                'label' => 'Akun Pengguna',
                'placeholder' => 'Pilih Pengguna',
                'type' => 'relationship',
                'name' => 'user_id',
                'entity' => 'user',
                'attribute' => 'email',
                'ajax' => true
            ]);
        } else {
            $this->crud->removeField('user_id');
        }

        if (!backpack_user()->hasRole('Developer') && !backpack_user()->hasRole('Admin')) {
            $this->crud->removeField('status');
        }

        $this->crud->modifyField('jenjang', [
            'label'       => "Jenjang",
            'type'        => 'select_from_array',
            'options'     => ['Semua' => 'Semua', 'SD' => 'SD', 'SMP' => 'SMP', 'SMK' => 'SMK'],
            'allows_null' => false,
            'default'     => '',
            // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
        ]);

        CRUD::modifyField('mapel_id', [
            'label' => 'Mata Pelajaran',
            'placeholder' => 'Pilih Mata Pelajaran',
            'type' => 'relationship',
            'name' => 'mapel_id',
            'entity' => 'mapel',
            'attribute' => 'nama',
            'ajax' => true
        ]);

        $this->crud->modifyField('kategori_id', [
            'type' => 'select2',
            'name' => 'kategori_id',
            'entity' => 'kategori', // the relationship name in your Model
            'attribute' => 'nama', // attribute on Article that is shown to admin,
        ]);

        $this->crud->modifyField('file_video', [
            'type' => 'upload',
            'name' => 'file_video',
            'src' => 'TEST',
            'upload'    => true,
        ]);

        $this->crud->modifyField('url_video_youtube', [
            'type' => 'url',
            'attributes' => [
                'placeholder' => 'Contoh : https://youtu.be/2zlyLDHfq8o',
            ],
        ]);

        $this->crud->modifyField('thumbnail', [
            'label'        => "Thumbnail",
            'name'         => "thumbnail",
            'type'         => 'image',
            'crop' => true, // set to true to allow cropping, false to disable
            'aspect_ratio' => 16/11,
        ]);

        $this->crud->modifyField('file_rpp', [
            'type' => 'upload',
            'name' => 'file_rpp',
            'upload'    => true,
        ]);
        
        $this->crud->modifyField('flow_diagram', [
            'type' => 'upload',
            'name' => 'flow_diagram',
            'upload'    => true,
        ]);

        $this->crud->modifyField('story_board', [
            'type' => 'upload',
            'name' => 'story_board',
            'upload'    => true,
        ]);
        
        if (backpack_user()->hasRole('Developer') && backpack_user()->hasRole('Admin')) {
            $this->crud->modifyField('status', [
                'label'       => "Status",
                'type'        => 'select_from_array',
                'options'     => ['Pending' => 'Pending', 'Aktif' => 'Aktif', 'Nonaktif' => 'Nonaktif'],
                'allows_null' => false,
                'default'     => 'Pending',
            ]);
        }

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $request = $this->crud->getRequest();
        if ($request->input('file_video')) {
            $this->crud->removeField('url_youtube');
        }
        $this->setupCreateOperation();
    }

    public function fetchUser()
    {
        // return $this->fetch(\App\Models\User::class);
        return $this->fetch([
            'model' => User::class,
            'searchable_attributes' => ['email', 'name'],
            'paginate' => 10,
            'query' => function ($query) {
                return $query->where('id', '!=', 1);
            }
        ]);
    }

    public function fetchMapel()
    {
        // return $this->fetch(\App\Models\User::class);
        return $this->fetch([
            'model' => Mapel::class,
            'searchable_attributes' => ['nama'],
            'paginate' => 10,
        ]);
    }
}
