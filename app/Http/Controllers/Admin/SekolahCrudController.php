<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SekolahRequest;
use App\Models\Sekolah;
use App\Models\User;
use App\Models\Wilayah\Kabupaten;
use App\Models\Wilayah\Kecamatan;
use App\Models\Wilayah\Kelurahan;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Route;
use Prologue\Alerts\Facades\Alert;

/**
 * Class SekolahCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class SekolahCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation {store as traitStore;}
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
  
        // if(Route::current()->parameter('id') != $user->sekolah()->first()->id){
        //     $this->crud->denyAccess('update');
        // }
        // if(backpack_user()->hasRole('Developer')){
        //     $this->crud->allowAccess('update');
        // }
        // if(!backpack_user()->hasRole('Developer')){
        //     $this->crud->denyAccess('create');
        //     $this->crud->denyAccess('delete');
        // }
        CRUD::setModel(\App\Models\Sekolah::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/sekolah');
        CRUD::setEntityNameStrings('Sekolah', 'Sekolah');
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        // CRUD::setFromDb(); // columns
        $this->crud->column('status')->type('text');
        $this->crud->column('nama_sekolah')->type('text');
        $this->crud->column('npsn')->type('text');
        $this->crud->column('jenjang')->type('text');
        $this->crud->column('nama_sekolah')->type('text');
        $this->crud->column('kepala_sekolah')->type('text');
        $this->crud->column('logo')->type('image');
        $this->crud->column('logo_dinas')->type('image');
        $this->crud->column('yayasan')->type('text');
        $this->crud->column('email')->type('text');
        $this->crud->column('no_telp')->type('text');

        $this->crud->modifyColumn('status', [
            'type'         => 'text',
            'label'        => 'Status', // Table column heading
            'wrapper' => [
                'element' => 'span',
                'class'   => function ($crud, $column, $entry, $related_key) {
                    if ($entry->status == 'Aktif') {
                        return 'badge badge-success';
                    }
                    return 'badge badge-default';
                },
            ],
        ]);
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(SekolahRequest::class);

        CRUD::setFromDb(); // fields

        $this->crud->modifyField('jenjang', [
            'label'       => "Jenjang",
            'type'        => 'select_from_array',
            'options'     => ['' => 'Pilih Jenjang', 'SD' => 'SD', 'SMP' => 'SMP', 'SMK' => 'SMK'],
            'allows_null' => false,
            'default'     => '',
            // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
        ]);

        $this->crud->modifyField('status_akreditasi', [
            'label'       => "Status Akreditasi",
            'type'        => 'select_from_array',
            'options'     => ['' => 'Pilih Status', 'A' => 'A', 'B' => 'B', 'C' => 'C', 'Belum Akreditasi' => 'Belum Akreditasi'],
            'allows_null' => false, 
            'default'     => '',
            // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
        ]);

        $this->crud->modifyField('logo', [
            'label'        => "Logo Sekolah",
            'name'         => "logo",
            'type'         => 'image',
            'crop' => true, // set to true to allow cropping, false to disable
            'aspect_ratio' => 1,
        ]);
        $this->crud->modifyField('logo_dinas', [
            'label'        => "Logo Dinas",
            'name'         => "logo_dinas",
            'type'         => 'image',
            'crop' => true, // set to true to allow cropping, false to disable
            'aspect_ratio' => 1,
        ]);
        $this->crud->modifyField('provinsi_id', [
            'type' => 'relationship',
            'name' => 'provinsi_id',
            'entity' => 'provinsi', // the relationship name in your Model
            'attribute' => 'name', // attribute on Article that is shown to admin,
        ]);

        $this->crud->modifyField('kabupaten_id', [
            'label' => 'Kabupaten',
            'name' => 'kabupaten_id',
            'type' => 'select2_from_ajax',
            'entity' => 'kabupaten',
            'model' => Kabupaten::class,
            'attribute' => 'name',
            'placeholder' => 'Pilih Kabupaten',
            'minimum_input_length' => 0,
            'dependencies' => ['provinsi_id'],
            'data_source' => route('getKabupatenAPI'),
            'method' => 'GET',
            'include_all_form_fields' => true
        ]);

        $this->crud->modifyField('kecamatan_id', [
            'label' => 'Kecamatan',
            'name' => 'kecamatan_id',
            'type' => 'select2_from_ajax',
            'entity' => 'kecamatan',
            'model' => Kecamatan::class,
            'attribute' => 'name',
            'placeholder' => 'Pilih Kecamatan',
            'minimum_input_length' => 0,
            'dependencies' => ['kabupaten_id'],
            'data_source' => url('api/wilayah/get-kecamatan'),
            'method' => 'GET',
            'include_all_form_fields' => true
        ]);

        $this->crud->modifyField('kelurahan_id', [
            'label' => 'Kelurahan',
            'name' => 'kelurahan_id',
            'type' => 'select2_from_ajax',
            'entity' => 'kelurahan',
            'model' => Kelurahan::class,
            'attribute' => 'name',
            'placeholder' => 'Pilih Kelurahan',
            'minimum_input_length' => 0,
            'dependencies' => ['kecamatan_id'],
            'data_source' => url('api/wilayah/get-kelurahan'),
            'method' => 'GET',
            'include_all_form_fields' => true
        ]);

        $this->crud->modifyField('status', [
            'label'       => "Status",
            'type'        => 'select_from_array',
            'options'     => ['Pending' => 'Pending', 'Aktif' => 'Aktif', 'Nonaktif' => 'Nonaktif'],
            'allows_null' => false,
            'default'     => 'Pending',
            // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
        ]);

        if(!backpack_user()->hasRole('Developer')){
            $this->crud->removeField('status');
        }
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        if(backpack_user()->hasRole('Developer') || backpack_user()->hasRole('Admin')){         
            $this->setupCreateOperation();
            $this->setupCreateOperation();
        }

    }
    public function store(){
        if($this->crud->getRequest()){
            $this->crud->setRequest($this->crud->validateRequest()); 
            $this->crud->unsetValidation();
            Alert::add('info', 'Sekolah & Akun berhasil dibuat (Password default adalah : NPSN)')->flash();
            return $this->traitStore();
        }else{
            return false;
        }
    }
}
