<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\TenpenRequest;
use App\Models\Tenpen;
use App\Models\User;
use App\Models\Wilayah\Kabupaten;
use App\Models\Wilayah\Kecamatan;
use App\Models\Wilayah\Kelurahan;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\Route;

/**
 * Class TenpenCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class TenpenCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Tenpen::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/tenpen');
        CRUD::setEntityNameStrings('Tenaga Pendidik', 'Tenaga Pendidik');

         if(!backpack_user()->hasRole('Developer')){
            $this->crud->denyAccess('create');
            $this->crud->denyAccess('delete');
        }
    }


    protected function setupListOperation()
    {
        // CRUD::setFromDb(); // columns
        // if (!backpack_user()->hasRole('Developer')) {
        //     $this->crud->addClause('whereHas', 'sekolah', function ($query) {
        //         $user = User::find(backpack_user()->id);
        //         $query->where('sekolah_id', $user->sekolah()->first()->id);
        //     });
        //     $this->crud->addClause('where', 'id', '!=', backpack_user()->id);
        // }
        // if (backpack_user()->hasRole('Developer')) {
        //     $this->crud->modifyColumn('sekolah_id', [
        //         'name'         => 'sekolah_id', // name of relationship method in the model
        //         'type'         => 'relationship',
        //         'label'        => 'Sekolah', // Table column heading
        //         'attribute'    => 'nama_sekolah'
        //     ]);
        // }else{
        //     $this->crud->removeColumn('sekolah_id');
        // }
        if(!backpack_user()->hasRole('Developer') && !backpack_user()->hasRole('Admin')){
            $this->crud->addClause('where', 'user_id', backpack_user()->id);
        }

        $this->crud->column('sekolah_id')->type('relationship')->attribute('nama_sekolah');
        $this->crud->column('foto')->type('image');
        $this->crud->column('nama_lengkap')->type('text');
        $this->crud->column('jenis_kelamin')->type('text');
        $this->crud->column('nik')->type('text');
        $this->crud->column('nuptk')->type('text');
        $this->crud->column('nip')->type('text');
        $this->crud->column('niy_nigk')->type('text');
        $this->crud->column('tmt')->type('text');
        $this->crud->column('tst')->type('text');
        $this->crud->column('no_telp')->type('text');
        $this->crud->column('email')->type('text');

        $this->crud->modifyColumn('nik', [
            'label'        => 'NIK', // Table column heading
        ]);
        $this->crud->modifyColumn('nuptk', [
            'label'        => 'NUPTK', // Table column heading
        ]);
        $this->crud->modifyColumn('nip', [
            'label'        => 'NIP', // Table column heading
        ]);
        $this->crud->modifyColumn('niy_nigk', [
            'label'        => 'NIY/NIGK', // Table column heading
        ]);
        $this->crud->modifyColumn('tmt', [
            'type'         => 'date',
            'label'        => 'TMT', // Table column heading
        ]);
        $this->crud->modifyColumn('tst', [
            'type'         => 'date',
            'label'        => 'TST', // Table column heading
        ]);
    }


    protected function setupCreateOperation()
    {
        CRUD::setValidation(TenpenRequest::class);
        CRUD::setFromDb(); // fields
        if(backpack_user()->hasRole('Developer')){
            $this->crud->modifyField('sekolah_id', [
                'type' => 'select2',
                'name' => 'sekolah_id',
                'entity' => 'sekolah', // the relationship name in your Model
                'attribute' => 'nama_sekolah', // attribute on Article that is shown to admin,
            ]);
            $this->crud->modifyField('user_id', [
                'type' => 'select2',
                'name' => 'user_id',
                'entity' => 'user', // the relationship name in your Model
                'attribute' => 'email', // attribute on Article that is shown to admin,
            ]);
        }else{
            $this->crud->removeField('sekolah_id');
            $this->crud->removeField('user_id');
        }
        $this->crud->modifyField('jenis_kelamin', [
            'label'       => "Jenis Kelamin",
            'type'        => 'select_from_array',
            'options'     => ['' => 'Pilih Jenis Kelamin', 'Laki-laki' => 'Laki-laki', 'Perempuan' => 'Perempuan'],
            'allows_null' => false,
            'default'     => '',
        ]);
        $this->crud->modifyField('agama', [
            'label'       => "Agama",
            'type'        => 'select_from_array',
            'options'     => [
                '' => 'Pilih Agama', 'Islam' => 'Islam', 'Kristen' => 'Kristen',
                'Katolik' => 'Katolik', 'Hindu' => 'Hindu', 'Buddha' => 'Buddha','Konghucu' => 'Konghucu'
            ],
            'allows_null' => false,
            'default'     => '',
        ]);
        $this->crud->modifyField('pendidikan', [
            'label'       => "Pendidikan",
            'type'        => 'select_from_array',
            'options'     => [
                '' => 'Pilih Pendiidkan', 'SD Sederajat' => 'SD Sederajat', 'SMA Sederajat' => 'SMA Sederajat',
                'D1' => 'D1', 'D2' => 'D2', 'D3' => 'D3', 'D4/S1' => 'D4/S1', 'S2' => 'S2', 'S3' => 'S3', 
            ],
            'allows_null' => false,
            'default'     => '',
        ]);
        $this->crud->modifyField('kewarganegaraan', [
            'label'       => "Kewarganegaraan",
            'type'        => 'select_from_array',
            'options'     => [
                '' => 'Pilih Pendiidkan', 'WNI' => 'WNI', 'WNA' => 'WNA'
            ],
            'allows_null' => false,
            'default'     => '',
        ]);
        $this->crud->modifyField('provinsi_id', [
            'type' => 'relationship',
            'name' => 'provinsi_id',
            'entity' => 'provinsi', // the relationship name in your Model
            'attribute' => 'name', // attribute on Article that is shown to admin,
        ]);

        $this->crud->modifyField('kabupaten_id', [
            'label' => 'Kabupaten',
            'name' => 'kabupaten_id',
            'type' => 'select2_from_ajax',
            'entity' => 'kabupaten',
            'model' => Kabupaten::class,
            'attribute' => 'name',
            'placeholder' => 'Pilih Kabupaten',
            'minimum_input_length' => 0,
            'dependencies' => ['provinsi_id'],
            'data_source' => route('getKabupatenAPI'),
            'method' => 'GET',
            'include_all_form_fields' => true
        ]);

        $this->crud->modifyField('kecamatan_id', [
            'label' => 'Kecamatan',
            'name' => 'kecamatan_id',
            'type' => 'select2_from_ajax',
            'entity' => 'kecamatan',
            'model' => Kecamatan::class,
            'attribute' => 'name',
            'placeholder' => 'Pilih Kecamatan',
            'minimum_input_length' => 0,
            'dependencies' => ['kabupaten_id'],
            'data_source' => url('api/wilayah/get-kecamatan'),
            'method' => 'GET',
            'include_all_form_fields' => true
        ]);

        $this->crud->modifyField('kelurahan_id', [
            'label' => 'Kelurahan',
            'name' => 'kelurahan_id',
            'type' => 'select2_from_ajax',
            'entity' => 'kelurahan',
            'model' => Kelurahan::class,
            'attribute' => 'name',
            'placeholder' => 'Pilih Kelurahan',
            'minimum_input_length' => 0,
            'dependencies' => ['kecamatan_id'],
            'data_source' => url('api/wilayah/get-kelurahan'),
            'method' => 'GET',
            'include_all_form_fields' => true
        ]);

        $this->crud->modifyField('foto', [
            'label'        => "Foto",
            'name'         => "foto",
            'type'         => 'image',
            'crop' => true, // set to true to allow cropping, false to disable
            'aspect_ratio' => 1,
        ]);
    }


    protected function setupUpdateOperation()
    {
        // if(!backpack_user()->hasRole('Developer')){
        //     $routeId = Route::current()->parameter('id');
        //     $identifyTenpenRoute = Tenpen::find($routeId);
        //     $routeIdTenpen = 'RouteIdTenpen';
        //     $userLoggedinSchool = 'User Loggedin School';
        //     if($identifyTenpenRoute){
        //         $routeIdTenpen = $identifyTenpenRoute->sekolah()->first()->id;
        //     }
        //     $identifyUserLoggedin = User::find(backpack_user()->id);
        //     if($identifyUserLoggedin){
        //         $userLoggedinSchool = $identifyUserLoggedin->sekolah()->first()->id;
        //     }
        //     if($routeIdTenpen == $userLoggedinSchool){
        //         $this->setupCreateOperation();
        //     }
        // }else{
        //     $this->setupCreateOperation();
        // }

        if(!backpack_user()->hasRole('Developer') && !backpack_user()->hasRole('Admin')){
            $tenpen = Tenpen::where('user_id', backpack_user()->id)->first();
            if(Route::current()->parameter('id') == $tenpen->id){
                $this->setupCreateOperation();
            }
        }else{
            $this->setupCreateOperation();
        }
    }
}
