<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Materi;
use Illuminate\Http\Request;

class MaterialController extends Controller
{
    public function index(){
        $title = 'Ruang Materi';
        $compact = compact('title');

        return view('material', $compact);
    }

    public function detail($slug){
        $materi = Materi::where('slug', $slug)->first();
        $title = $materi->judul;
        $compact = compact('title','materi');

        return view('material_detail', $compact);
    }

}
