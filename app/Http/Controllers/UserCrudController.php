<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\Role;
use App\Models\Sekolah;
use App\Models\User;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\PermissionManager\app\Http\Requests\UserStoreCrudRequest as StoreRequest;
use Backpack\PermissionManager\app\Http\Requests\UserUpdateCrudRequest as UpdateRequest;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;

class UserCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation {
        store as traitStore;
    }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation {
        update as traitUpdate;
    }
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\BulkDeleteOperation;

    public function setup()
    {
        if (Route::current()->parameter('id') == 1) {
            $this->crud->denyAccess('delete');
            $this->crud->denyAccess('update');
        }
        if(!backpack_user()->hasRole('Developer')){
            $this->crud->denyAccess('create');
        }
        $this->crud->setModel(config('backpack.permissionmanager.models.user'));
        $this->crud->setEntityNameStrings(trans('backpack::permissionmanager.user'), trans('backpack::permissionmanager.users'));
        $this->crud->setRoute(backpack_url('user'));
    }

    public function setupListOperation()
    {
        $this->crud->addColumns([
            [
                'name'  => 'name',
                'label' => trans('backpack::permissionmanager.name'),
                'type'  => 'text',
            ],
            [
                'name'  => 'email',
                'label' => trans('backpack::permissionmanager.email'),
                'type'  => 'email',
            ],
            [ // n-n relationship (with pivot table)
                'label'     => trans('backpack::permissionmanager.roles'), // Table column heading
                'type'      => 'select_multiple',
                'name'      => 'roles', // the method that defines the relationship in your Model
                'entity'    => 'roles', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'model'     => config('permission.models.role'), // foreign key model
            ],
            [ // n-n relationship (with pivot table)
                'label'     => trans('backpack::permissionmanager.extra_permissions'), // Table column heading
                'type'      => 'select_multiple',
                'name'      => 'permissions', // the method that defines the relationship in your Model
                'entity'    => 'permissions', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'model'     => config('permission.models.permission'), // foreign key model
            ],
        ]);

        // Role Filter
        $this->crud->addFilter(
            [
                'name'  => 'role',
                'type'  => 'dropdown',
                // 'label' => trans('backpack::permissionmanager.role'),
                'label' => trans('Hak Akses'),
            ],
            Role::pluck('name', 'id')->toArray(),
            function ($value) { // if the filter is active
                $this->crud->addClause('whereHas', 'roles', function ($query) use ($value) {
                    $query->where('role_id', '=', $value);
                });
            }
        );

        // $this->crud->addField([
        //     'label' => 'Sekolah',
        //     'name' => 'sekolah',
        //     'type' => 'select2_from_ajax',
        //     'entity' => 'Sekolah',
        //     'model' => Sekolah::class,
        //     'attribute' => 'nama_sekolah',
        //     'placeholder' => 'Pilih Sekolah',
        //     'minimum_input_length' => 2,
        //     'dependencies' => ['kecamatan_id'],
        //     'data_source' => url('api/sekolah/list'),
        //     'method' => 'GET',
        //     // 'include_all_form_fields' => true
        //     // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
        // ]);

        // Extra Permission Filter
        // $this->crud->addFilter(
        //     [
        //         'name'  => 'permissions',
        //         'type'  => 'select2',
        //         'label' => trans('backpack::permissionmanager.extra_permissions'),
        //     ],
        //     config('permission.models.permission')::all()->pluck('name', 'id')->toArray(),
        //     function ($value) { // if the filter is active
        //         $this->crud->addClause('whereHas', 'permissions', function ($query) use ($value) {
        //             $query->where('permission_id', '=', $value);
        //         });
        //     }
        // );
    }

    public function setupCreateOperation()
    {
        // if(backpack_user()->hasRole('Developer')){
        //     $this->crud->addField([
        //         'type' => 'relationship',
        //         'name' => 'sekolah',
        //         'entity' => 'sekolah', // the relationship name in your Model
        //         'attribute' => 'nama_sekolah', // attribute on Article that is shown to admin,
        //     ]);
        // }
        $this->addUserFields();
        $this->crud->setValidation(UserRequest::class);
    }

    public function setupUpdateOperation()
    {   
        // if(!backpack_user()->hasRole('Developer')){
        //     $routeId = Route::current()->parameter('id');
        //     $identifyUserRoute = User::find($routeId);
        //     $routeIdSchool = 'RouteIdSchool';
        //     $userLoggedinSchool = 'User Loggedin School';
        //     if($identifyUserRoute){
        //         $routeIdSchool = $identifyUserRoute->sekolah()->first()->id;
        //     }
    
        //     $identifyUserLoggedin = User::find(backpack_user()->id);
        //     if($identifyUserLoggedin){
        //         $userLoggedinSchool = $identifyUserLoggedin->sekolah()->first()->id;
        //     }
    
        //     if($routeIdSchool == $userLoggedinSchool){
        //         $this->addUserFields();
        //     }
        // }else{
        // }
        $this->addUserFields();
        $this->crud->setValidation(UpdateRequest::class);
    }

    /**
     * Store a newly created resource in the database.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $this->crud->setRequest($this->crud->validateRequest());
        $this->crud->setRequest($this->handlePasswordInput($this->crud->getRequest()));
        $this->crud->unsetValidation(); // validation has already been run

        return $this->traitStore();
    }

    /**
     * Update the specified resource in the database.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update()
    {
        $this->crud->setRequest($this->crud->validateRequest());
        $this->crud->setRequest($this->handlePasswordInput($this->crud->getRequest()));
        $this->crud->unsetValidation(); // validation has already been run

        return $this->traitUpdate();
    }

    /**
     * Handle password input fields.
     */
    protected function handlePasswordInput($request)
    {
        // Remove fields not present on the user.
        $request->request->remove('password_confirmation');
        $request->request->remove('roles_show');
        $request->request->remove('permissions_show');

        // Encrypt password if specified.
        if ($request->input('password')) {
            $request->request->set('password', Hash::make($request->input('password')));
        } else {
            $request->request->remove('password');
        }

        return $request;
    }


    protected function addUserFields()
    {
        $this->crud->addFields([
            [
                'name'  => 'name',
                'label' => trans('backpack::permissionmanager.name'),
                'type'  => 'text',
            ],
            [
                'name'  => 'email',
                'label' => trans('backpack::permissionmanager.email'),
                'type'  => 'email',
            ],
            [
                'name'  => 'password',
                'label' => trans('backpack::permissionmanager.password'),
                'type'  => 'password',
            ],
            [
                'name'  => 'password_confirmation',
                'label' => trans('backpack::permissionmanager.password_confirmation'),
                'type'  => 'password',
            ],
            [
                // two interconnected entities
                'label'             => trans('backpack::permissionmanager.user_role_permission'),
                'field_unique_name' => 'user_role_permission',
                'type'              => 'checklist_dependency',
                'name'              => ['roles', 'permissions'],
                'subfields'         => [
                    'primary' => [
                        'label'            => trans('backpack::permissionmanager.roles'),
                        'name'             => 'roles', // the method that defines the relationship in your Model
                        'entity'           => 'roles', // the method that defines the relationship in your Model
                        'entity_secondary' => 'permissions', // the method that defines the relationship in your Model
                        'attribute'        => 'name', // foreign key attribute that is shown to user
                        'model'            => Role::class, // foreign key model
                        'pivot'            => true, // on create&update, do you need to add/delete pivot table entries?]
                        'number_columns'   => 3, //can be 1,2,3,4,6,
                    ],
                    'secondary' => [
                        // 'label'          => ucfirst(trans('backpack::permissionmanager.permission_singular')),
                        'label'          => 'Permissions',
                        'name'           => 'permissions', // the method that defines the relationship in your Model
                        'entity'         => 'permissions', // the method that defines the relationship in your Model
                        'entity_primary' => 'roles', // the method that defines the relationship in your Model
                        'attribute'      => 'name', // foreign key attribute that is shown to user
                        'model'          => config('permission.models.permission'), // foreign key model
                        'pivot'          => true, // on create&update, do you need to add/delete pivot table entries?]
                        'number_columns' => 3, //can be 1,2,3,4,6,
                    ],
                ],
            ],

        ]);
    }
}
