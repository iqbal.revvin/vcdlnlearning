<?php

namespace App\Http\Controllers\API\MateriService;

use App\Helpers\GlobalHelper;
use App\Http\Controllers\Controller;
use App\Models\Kategori;
use App\Models\Mapel;
use App\Models\Materi;
use App\Models\Tenpen;
use Illuminate\Http\Request;

class MateriServiceController extends Controller
{
    public function index(Request $request){
        $paramOffset = $request->input('offset');
        $materi = Materi::with('user')->with('kategori')->with('mapel')->where('status', 'Aktif')->limit(1200)->offset($paramOffset)->latest('created_at')->get();
        $materi = $materi->map(function($item, $key){
            // return collect($item)->except(['user_id'])->toArray();
            $photo = Tenpen::where('user_id', $item->user->id)->pluck('foto')->first();
            return [
                'id' => $item->id,
                'judul' => $item->judul,
                'kategori' => $item->kategori->nama,
                'thumbnail' => $item->thumbnail,
                'file_video' => $item->file_video,
                'mapel' => $item->mapel->nama,
                'user' => $item->user->name,
                'photo_profile' => $photo
            ];
        });
        if($materi){
            return GlobalHelper::createResponse(true, 'Success Get Data', $materi);
        }else{
            return GlobalHelper::createResponse(false, 'Unknown error');
        }
    }

    public function getCategoryByJenjang(Request $request){
        $category = Kategori::whereHas('materi', function($query) use ($request)  {
            return $query->where('jenjang', $request->input('jenjang'));
        })->get();
        $category = $category->map(function($item, $key){
            return collect($item)->except(['created_at', 'updated_at'])->toArray();
        });
        if($category){
            return GlobalHelper::createResponse(true, 'Success Get Data', $category);
        }else{
            return GlobalHelper::createResponse(false, 'Failed Get Data');
        }
    }

    public function getMapelByKategori(Request $request){
        $mapel = Mapel::whereHas('materi', function($query) use ($request)  {
            return $query->where('kategori_id', $request->input('kategoriId'));
        })->get();
        $mapel = $mapel->map(function($item, $key){
            return collect($item)->except(['created_at', 'updated_at'])->toArray();
        });
        if($mapel){
            return GlobalHelper::createResponse(true, 'Success Get Data', $mapel);
        }else{
            return GlobalHelper::createResponse(false, 'Failed Get Data');
        }
    }

}
