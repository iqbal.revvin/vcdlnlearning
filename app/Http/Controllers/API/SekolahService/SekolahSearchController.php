<?php

namespace App\Http\Controllers\API\SekolahService;

use App\Helpers\GlobalHelper;
use App\Http\Controllers\Controller;
use App\Models\Sekolah;
use Illuminate\Http\Request;

class SekolahSearchController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $search_term = $request->input('q');
        $options = Sekolah::query();

        if($search_term){
            $options->where('nama_sekolah', 'LIKE', '%'.$search_term.'%')->orWhere('npsn', 'LIKE', '%'.$search_term.'%');
        }else{
            return GlobalHelper::createResponse(false, 'Masukan kata kunci pencarian');
        }
        if($options->count() != 0){
            return GlobalHelper::createResponse(true, 'Data sekolah ditemukan', $options->limit(50)->get());
        }else{
            return GlobalHelper::createResponse(false, 'Sekolah tidak ditemukan');
        }
    }
}
