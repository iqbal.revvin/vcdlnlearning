<?php

namespace App\Http\Controllers\API\SekolahService;

use App\Http\Controllers\Controller;
use App\Models\Sekolah;
use Illuminate\Http\Request;

class SekolahListController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $search_term = $request->input('q');
        $options = Sekolah::query();

        if ($search_term) {
             $options->where('nama_sekolah', 'LIKE', '%'.$search_term.'%');
        }else{
            return Sekolah::paginate(10);
        }

        return $options->paginate(10);
    }
}
