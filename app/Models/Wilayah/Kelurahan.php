<?php

namespace App\Models\Wilayah;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kelurahan extends Model
{
    use HasFactory;

    protected $table = 'kelurahan';
    protected $guarded = ['id'];

    public function kecamatan(){
        $this->belongsToMany(Kecamatan::class);
    }
}
