<?php

namespace App\Models\Wilayah;

use App\Models\Sekolah;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Provinsi extends Model
{
    use HasFactory, CrudTrait;

    protected $table = 'provinsi';
    // protected $guarded = ['id'];

    public function sekolah(){
        return $this->belongsToMany(Sekolah::class);
    }

    public function kabupaten(){
        return $this->hasMany(Kabupaten::class);
    }
}
