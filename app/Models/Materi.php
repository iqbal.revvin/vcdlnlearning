<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Str;

class Materi extends Model
{
    use HasFactory;
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'materi';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function boot()
    {
        parent::boot();
        static::deleting(function($obj) {
            $disk = config('backpack.base.root_disk_name');
            // dd('public/'.$obj->thumbnail);
            Storage::disk($disk)->delete($obj->file_video);
            Storage::disk($disk)->delete($obj->file_rpp);
            Storage::disk($disk)->delete($obj->flow_diagram);
            Storage::disk($disk)->delete($obj->story_board);
            Storage::disk($disk)->delete('public/'.$obj->thumbnail);
        });
    }

    public function getYoutubeLink()
    {
        return '<a href="' . $this->url_video_youtube. '" target="_blank">' . $this->url_video_youtube . '</a>';
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function user(){
        return $this->belongsTo(User::class);
    }

    public function mapel(){
        return $this->belongsTo(Mapel::class);
    }

    public function kategori(){
        return $this->belongsTo(Kategori::class);
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    public function setFileVideoAttribute($value)
    {
        $attribute_name = "file_video";
        $disk = config('backpack.base.root_disk_name');
        $destination_path = "public/uploads/videos";
        $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path);
    }

    public function setFileRppAttribute($value)
    {
        $attribute_name = "file_rpp";
        $disk = config('backpack.base.root_disk_name');
        $destination_path = "public/uploads/rpp";
        $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path);
    }

    public function setFlowDiagramAttribute($value)
    {
        $attribute_name = "flow_diagram";
        $disk = config('backpack.base.root_disk_name');
        $destination_path = "public/uploads/rpp";
        $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path);
    }
    public function setStoryBoardAttribute($value)
    {
        $attribute_name = "story_board";
        $disk = config('backpack.base.root_disk_name');
        $destination_path = "public/uploads/rpp";
        $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path);
    }

    public function setThumbnailAttribute($value)
    {
        $attribute_name = "thumbnail";
        $disk = config('backpack.base.root_disk_name');
        $destination_path = "public/uploads/images/thumbnail";

        if ($value == null) {
            Storage::disk($disk)->delete($this->{$attribute_name});
            $this->attributes[$attribute_name] = null;
        }

        if (Str::startsWith($value, 'data:image')) {
            $image = Image::make($value)->encode('png', 90);
            $filename = md5($value . time()) . '.png';
            Storage::disk($disk)->put($destination_path . '/' . $filename, $image->stream());
            Storage::disk($disk)->delete($this->{$attribute_name});
            $public_destination_path = Str::replaceFirst('public/', '', $destination_path);
            // $public_destination_path = $destination_path; 
            $this->attributes[$attribute_name] = $public_destination_path . '/' . $filename;
        }
    }
}
