<?php

namespace App\Observers;

use App\Models\Materi;
use Illuminate\Support\Str;

class MateriObserver
{
    /**
     * Handle the Materi "created" event.
     *
     * @param  \App\Models\Materi  $materi
     * @return void
     */
    public function created(Materi $materi)
    {
        if(!backpack_user()->hasRole('Developer') || !backpack_user()->hasRole('Admin')){
            $materi->user_id = backpack_user()->id;
        }
        $materi->slug = Str::slug($materi->judul);
        $materi->save();
    }

    /**
     * Handle the Materi "updated" event.
     *
     * @param  \App\Models\Materi  $materi
     * @return void
     */
    public function updated(Materi $materi)
    {
        Materi::where('id', $materi->id)->update([
            'slug' => Str::slug($materi->judul),
        ]);
    }

    /**
     * Handle the Materi "deleted" event.
     *
     * @param  \App\Models\Materi  $materi
     * @return void
     */
    public function deleted(Materi $materi)
    {
        //
    }

    /**
     * Handle the Materi "restored" event.
     *
     * @param  \App\Models\Materi  $materi
     * @return void
     */
    public function restored(Materi $materi)
    {
        //
    }

    /**
     * Handle the Materi "force deleted" event.
     *
     * @param  \App\Models\Materi  $materi
     * @return void
     */
    public function forceDeleted(Materi $materi)
    {
        //
    }
}
