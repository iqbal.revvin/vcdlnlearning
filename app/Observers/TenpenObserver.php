<?php

namespace App\Observers;

use App\Models\Tenpen;
use App\Models\User;

class TenpenObserver
{
    /**
     * Handle the Tenpen "created" event.
     *
     * @param  \App\Models\Tenpen  $tenpen
     * @return void
     */
    public function created(Tenpen $tenpen)
    {
        //
    }

    /**
     * Handle the Tenpen "updated" event.
     *
     * @param  \App\Models\Tenpen  $tenpen
     * @return void
     */
    public function updated(Tenpen $tenpen)
    {
        //
    }

    /**
     * Handle the Tenpen "deleted" event.
     *
     * @param  \App\Models\Tenpen  $tenpen
     * @return void
     */
    public function deleted(Tenpen $tenpen)
    {
       
    }

     /**
     * Handle the Tenpen "deleting" event.
     *
     * @param  \App\Models\Tenpen  $tenpen
     * @return void
     */
    public function deleting(Tenpen $tenpen)
    {
        $user = User::query();
        $user_exists = $user->where('id', $tenpen->user_id)->exists();
        if($user_exists){
            $user->where('id', $tenpen->user_id)->delete();
        }
        // dd($tenpen->user_id);
    }
  

    /**
     * Handle the Tenpen "restored" event.
     *
     * @param  \App\Models\Tenpen  $tenpen
     * @return void
     */
    public function restored(Tenpen $tenpen)
    {
        //
    }

    /**
     * Handle the Tenpen "force deleted" event.
     *
     * @param  \App\Models\Tenpen  $tenpen
     * @return void
     */
    public function forceDeleted(Tenpen $tenpen)
    {
        //
    }
}
