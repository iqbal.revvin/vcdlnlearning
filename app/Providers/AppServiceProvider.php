<?php

namespace App\Providers;

use App\Models\Materi;
use App\Models\Role;
use App\Models\Tenpen;
use App\Models\User;
use App\Observers\MateriObserver;
use App\Observers\RoleObserver;
use App\Observers\TenpenObserver;
use App\Observers\UserObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Role::observe(RoleObserver::class);
        User::observe(UserObserver::class);
        Tenpen::observe(TenpenObserver::class);
        Materi::observe(MateriObserver::class);
    }
}
