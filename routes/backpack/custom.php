<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

use Illuminate\Support\Facades\Route;


Route::group(['prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => array_merge((array) config('backpack.base.web_middleware', 'web'),['web', backpack_middleware()]),
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::group(['middleware' => ['can:Sekolah']], function () {
        Route::crud('sekolah', 'SekolahCrudController');
    });
    Route::group(['middleware' => ['can:Tenaga Pendidik']], function () {
        Route::crud('tenpen', 'TenpenCrudController');
    });
    Route::group(['middleware' => ['can:Mata Pelajaran']], function () {
        Route::crud('mapel', 'MapelCrudController');
    });
    Route::group(['middleware' => ['can:Materi']], function () {
        Route::crud('materi', 'MateriCrudController');
    });
    
    Route::crud('kategori', 'KategoriCrudController');
});