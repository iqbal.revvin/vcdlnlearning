const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js').react()
    .postCss("resources/css/app.css", "public/css", [
        require("tailwindcss")('./tailwind.config.js'),
    ]);
    // .browserSync('http://localhost:8000');

if (mix.inProduction()) {
    mix.version();
}
    
mix.js('resources/js/app.jsx', 'public/js/module').react()
// mix.copy('node_modules/@fortawesome/fontawesome-free/css/all.min.css', 'public/webfonts');

mix.copy('node_modules/@fortawesome/fontawesome-free/webfonts', 'public/webfonts');
